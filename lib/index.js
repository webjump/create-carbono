const clone = require('./clone');
const path = require('path');
const questions = require('./questions');
const execa = require('execa');
const { info } = require('./log');
const packageInfo = require('../package.json');
const updatePackageJson = require('./updatePackageJson');

const getIsYarn = () => {
  const userAgent = process.env.npm_config_user_agent || '';
  return userAgent.includes('yarn');
}

const getAbsoluteDirectory = directory => {
  let dir = path.normalize(directory);
  if (dir[0] === '~') {
    dir = path.join(process.env.HOME, dir.slice(1));
  }

  const isAbsolute = path.isAbsolute(dir);
  return isAbsolute ? dir : path.resolve(`./${directory}`);
}

module.exports = async () => {
  const isYarn = getIsYarn();

  info(`\n@webjump/create-carbono - ${packageInfo.version}`);

  const { directory: dir, projectName } = await questions();
  const directory = getAbsoluteDirectory(dir);

  const execOnDir = async (command, options) => execa(command, options, { cwd: directory });

  await clone({ directory, isYarn, execOnDir });
  await updatePackageJson({ directory, execOnDir, packageInfo: {
    name: projectName
  }})
}
