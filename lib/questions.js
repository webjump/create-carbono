const { basename } = require('path');
const inquirer = require('inquirer');
const isInvalidPath = require('is-invalid-path');

const questions = [
  {
    name: 'directory',
    message:
      'Project directory',
    validate: dir =>
      !dir
        ? 'Please enter a directory path'
        : isInvalidPath(dir)
        ? 'Invalid directory path; contains illegal characters'
        : true
  },
  {
    name: 'projectName',
    message: 'Project name',
    default: ({ directory }) => basename(directory)
  },
];

module.exports = async () => await inquirer.prompt(questions);
