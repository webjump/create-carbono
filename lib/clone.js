const execa = require('execa');
const { info, success } = require('./log');

const CARBONO_REPO = 'git@bitbucket.org:webjump/project-carbono-colmeia.git';

module.exports = async ({ directory = '.', isYarn, execOnDir }) => {
  info(`Cloning base repo`);
  await execa(`git`, ['clone', CARBONO_REPO, directory]);
  success(`Base repo cloned into: ${directory}`);

  info('Installing dependencies');
  await execOnDir(isYarn ? 'yarn' : 'npm', ['install']);
  success('Dependencies installed successfully');
};
