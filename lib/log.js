const chalk = require('chalk');
const log = console.log;

module.exports = {
  info: (...props) => log(chalk.white.bold(...props, '\n')),
  success: (...props) => log(chalk.green.bold(...props, '\n')),
  error: (...props) => log(chalk.red.bold(...props, '\n')),
}
