const fse = require('fse');

module.exports = async ({ directory, packageInfo }) => {
  const packagePath = `${directory}/package.json`;
  const basePackage = require(packagePath);

  const updatedPackageJson = {
    ...basePackage,
    ...packageInfo,
  }

  await fse.writeFile(packagePath, JSON.stringify(updatedPackageJson, null, 2));
};
